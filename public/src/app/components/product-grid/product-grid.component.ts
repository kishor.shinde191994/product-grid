import { Component, ComponentFactoryResolver, OnInit, ViewContainerRef } from '@angular/core';
import { Product } from '../../interfaces/product.interface';
import { ProductsService } from '../../services/products.service';
import { LoadingComponent } from '../loading/loading.component';

@Component({
  selector: 'product-grid',
  templateUrl: './product-grid.component.html',
  styleUrls: ['./product-grid.component.css']
})
export class ProductGridComponent implements OnInit {
  products: Product[] = [];
  chunkSize = 10;
  orderBy = undefined;
  noOfProducts = 500;
  loaderComponentInstance: any;
  nextBatchProducts: Product[];
  constructor(private productService: ProductsService, 
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver) { }

  
  ngOnInit(): void {
    let context = this;
    this.fetchProducts()
    // eventListener on select to detect change in orderBy field
    document.getElementById('orderByFields').addEventListener('change', function(event) {
      context.orderBy = (event.target as any).value;
      context.products = [];
      context.fetchProducts();
    });
  }

  showLoading() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(LoadingComponent);
    this.loaderComponentInstance = this.viewContainerRef.createComponent(componentFactory);
    const loaderComponentElement = this.loaderComponentInstance.location.nativeElement;
    document.getElementById('product-list').parentNode.insertBefore(loaderComponentElement, document.getElementById('product-list').nextSibling)
    document.querySelector('#loader').scrollIntoView()
  }

  hideLoading() {
    this.loaderComponentInstance.destroy();
  }

  // used for trackBy in ngFor to create only newly inserted products in DOM
  identify(product: Product) {
    return product.id
  }

  fetchProducts(isNext: boolean = false) {
    if(this.products.length < this.noOfProducts) {
      if(!isNext) {
        this.showLoading();
      }   
      this.nextBatchProducts = []
      this.productService.getProducts((this.products.length/this.chunkSize) + 1, this.chunkSize, this.orderBy)
        .subscribe(resp => {
          if(!isNext) { 
            this.hideLoading()
            this.products = this.products.concat(resp.body);
            this.fetchProducts(true)
          } else {
            this.nextBatchProducts = resp.body;
          }          
        });
    } else {
      this.appendEndMessage()
    }    
  }

  appendEndMessage() {
    if(!document.getElementById('endMessage')) {
      var div = document.createElement("div");
      div.setAttribute("id", "endMessage");
      div.innerText = '~ end of catalogue ~';
      div.style.textAlign = 'center';
      document.getElementById('product-list').appendChild(div);
    }   
  }

  setNextBatch() {
    if(this.products.length < this.noOfProducts) {
      this.products = this.products.concat(this.nextBatchProducts);
      this.fetchProducts(true)
    } else {
      this.nextBatchProducts = []
    }
  }

  onScrollingFinished() {
    if(!document.querySelector('#loader')) {
      if(this.nextBatchProducts && this.nextBatchProducts.length) {
        this.setNextBatch()
      } else {
        this.fetchProducts(false)
      }      
    }    
  }
}
