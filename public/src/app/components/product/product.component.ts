import { Component, Input } from '@angular/core';
import { Product } from '../../interfaces/product.interface';

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  @Input() product: Product;

  getFormattedPrice() {
    return '$' + this.product.price/100;
  }

  getFormattedDate() {
    let date = Math.ceil((new Date().getTime() - new Date(this.product.date).getTime())/(1000 * 60 * 60 * 24));
    if( date < 7) {
      return date + ' days ago';
    } else {      
      return this.product.date;
    }
  }
}
