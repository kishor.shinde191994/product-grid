import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[ScrollTracker]'
})
export class ScrollTrackerDirective {
  @Output() scrollingFinished = new EventEmitter<void>();
  emitted = false;

  @HostListener("window:scroll", [])
  onScroll(): void {
    if (!this.emitted && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.emitted = true;
      this.scrollingFinished.emit();
      this.emitted = true;
    } else if ((window.innerHeight + window.scrollY) < document.body.offsetHeight) {
      this.emitted = false;
    }
  }
}