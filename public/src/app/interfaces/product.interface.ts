export interface Product {
  id: string;
  date: string;
  face: string;
  price: number;
  size: number;
}
