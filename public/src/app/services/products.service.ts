import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Product } from '../interfaces/product.interface';
import { environment } from '../../environments/environment';

const API_ENDPOINT = environment.api_endpoint;

export enum EndPointsURI {
  'product' = '/api/products'
}

@Injectable({
  providedIn: 'root'
})

export class ProductsService {  
  constructor(private http: HttpClient) { }

  getProducts(page: number, limit: number, sort: string): Observable<HttpResponse<Product[]>> {
    let params = '?'
    if(page) {
      params = params + '_page=' + page
    }
    if(limit) {
      params = params + '&_limit=' + limit
    }
    if(sort) {
      params = params + '&_sort=' + sort
    }
    return this.http.get<Product[]>(
      API_ENDPOINT + EndPointsURI.product + params, { observe: 'response' }
    ).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }
}


